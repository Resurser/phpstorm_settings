# README #

PhpStorm 2016.3 settings for PHP projects

### What is this repository for? ###

* Settings for PHP coding with HTML, JS, CSS, Vargant, DB support
* PHP code style PSR-0, PSR-1, PSR-2, PSR-3, PSR-4
* Optimized for low-end PCs

### How do I get set up? ###

* Setup: PhpStorm->File->Settings Repository (https://bitbucket.org/Resurser/phpstorm_settings)