<?php
/*
 * Project: ${PROJECT_NAME};
 * Creator: ${USER}; 
 *    Date: ${DATE};
 */

#if (${NAMESPACE})

namespace ${NAMESPACE}{
	
	class ${NAME} {

	}
}

#else

class ${NAME} {

}
#end

