<?php
/*
 Project : ${PROJECT_NAME};
 Author  : ${USER}; 
 Created : ${DATE};
*/
 
#if (${NAMESPACE})

namespace ${NAMESPACE} {
	
	trait ${NAME} {

	}
}
#else

interface ${NAME} {

}

#end
